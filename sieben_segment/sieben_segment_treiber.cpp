#include <Arduino.h>
#include "sieben_segment_treiber.h"

SiebenSegment::SiebenSegment(bool ca, int* ziffer_pins, int* segment_pins) {
    lastStep = millis();

    for(byte i = 0; i < 4; i++) {
        digitPins[i] = ziffer_pins[i];
    }
    for(byte i = 0; i < 8; i++) {
        segmentPins[i] = segment_pins[i];
    }
    currentNumber = 0;
    currentPlace = 0;
    decimalPoint = 5;
    number = true;
    textSpeed = 1000;

    high = (ca) ? HIGH : LOW;
    low = (ca) ? LOW : HIGH;
};

void SiebenSegment::run() {
    if((lastStep + 1) < millis()) {
        for(byte i = 0; i < 8; i++) {
            digitalWrite(segmentPins[i], high);
        }
        for(byte i = 0; i < 4; i++) {
            if(currentPlace==i)
                digitalWrite(digitPins[i], high);
            else
                digitalWrite(digitPins[i], low);
        }

        unsigned int litNumber;
        char letter;
        if(number) {
            unsigned int powerOfTen;
            switch(currentPlace) {
                case 0:
                    powerOfTen = 1;
                    break;
                case 1:
                    powerOfTen = 10;
                    break;
                case 2:
                    powerOfTen = 100;
                    break;
                case 3:
                    powerOfTen = 1000;
                    break;
            }
            litNumber = (currentNumber / powerOfTen) % 10;
            /*
                unsigned a = 3242;
                    vorletzte ziffer extrahieren (2te von rechts):
                unsigned x = (3242 / 10^1) % 10     
                    x=4, da 324 % 10 (Modulo teilt durch 10 und gibt ganzzahligen Rest zurück) = 4
            */
        } else {
            if((lastTextShift+textSpeed)<millis()) {
                if(textPosition==textLength+3) textPosition=0;
                else textPosition++;
                lastTextShift=millis();
            }

            letter = text.charAt(textPosition-currentPlace);
            letter = (letter==32) ? 26 : letter-65;
            if(letter<0 || letter>26) letter = 26;
        }


        for(int i = 0; i < 8; i++) { //zählt von segment a bis p hoch
            bool segmentLit = (number) ? (((digits[litNumber]) >> (i)) & 0x01) : (((letters[letter]) >> (i)) & 0x01);
            if(segmentLit)
                digitalWrite(segmentPins[i], low);
        }
        if(decimalPoint==currentPlace) {
            digitalWrite(segmentPins[7], low);
        }

        lastStep = millis();
        currentPlace = (currentPlace > 2) ? 0 : currentPlace + 1;
    }
};

void SiebenSegment::drawNumber(int number, unsigned char decimal_point) {
    currentNumber = number;
    decimalPoint = decimal_point;
    number = true;
};

void SiebenSegment::drawNumber(int number) {
    currentNumber = number;
    number = true;
};

void SiebenSegment::drawDecimal(unsigned char decimal_point) {
    decimalPoint = decimal_point;
    number = true;
};

void SiebenSegment::init() {
    for(byte i = 0; i < 4; i++) {
        pinMode(digitPins[i], OUTPUT);
    }
    for(byte i = 0; i < 8; i++) {
        pinMode(segmentPins[i], OUTPUT);
    }
};

void SiebenSegment::drawText(String drawText) {
    drawText.toUpperCase();
    text = drawText;
    textLength = text.length();
    textPosition = 0;
    number = false;
}

void SiebenSegment::setTextSpeed(int setTextSpeed) {
    textSpeed = setTextSpeed;
}