/*
########--------AUTOR--------########
#####------Maximilian Heeß------#####
*/

#include "sieben_segment_treiber.h"


int zifferPins[4] = {2, 3, 4, 5};                      //1, 2, 3, 4
int segmentPins[8] = {6, 7, 8, 9, 10, 11, 12, 13};     //a, b, c, d, e, f, g, p

SiebenSegment ss(COMMON_CATHODE, zifferPins, segmentPins);

unsigned long lastSet = 0;
byte counter = 0;           //Zähler
int countInterval = 1000;   //Zeitabstand, in welchem hochgezählt wird

void setup() {
    Serial.begin(115200);
    ss.init();                  //Setzt Outputs
    ss.drawText("Hallo Welt");
    ss.setTextSpeed(1000);
}

void loop() {
    ss.run();                   //Damit wird die Zahl auf die Anzeige automatisch Projiziert

    /*if((lastSet+countInterval)<millis()) {  //Zeitabstand ohne delay() Funktion
        ss.drawNumber(counter);             //"Malt" die Zahlen
        //ss.drawNumber(counter, DEZIMALPUNKT); DEZIMALPUNKT beschreibt die Stelle, an welcher
        //der Dezimalpunkt dargestellt wird

        //ss.drawDecimal(DEZIMALPUNKT)


        if(counter==9999)
            counter=0;
        else
            counter++;

        lastSet = millis();
    }*/
}