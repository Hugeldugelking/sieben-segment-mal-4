#include <Arduino.h>

#ifndef sieben_segment_treiber_h
#define sieben_segment_treiber_h

#define COMMON_CATHODE 0
#define COMMON_ANODE 1

class SiebenSegment {

    private:
        long
            lastStep,
            lastTextShift;
        int
            textPosition,
            textSpeed,
            textLength,
            digitPins[4],
            segmentPins[8],
            currentPlace;
        unsigned char
            decimalPoint,
            digits[10] = {
                B00111111,
                B00000110,
                B01011011,
                B01001111,
                B01100110,
                B01101101,
                B01111101,
                B00000111,
                B01111111,
                B01101111
            }, //alle segmentaufstellungen für zahlen von 0 bis 9 (pgfedcba)
            letters[27] = {
                B01110111,
                B01111100,
                B00111001,
                B01011110,
                B01111001,
                B01110001,
                B00111101,
                B01110110,
                B00000110,
                B00011110,
                B01110101,
                B00111000,
                B00010101,
                B01010100,
                B00111111,
                B01110011,
                B10111111,
                B01110101,
                B01101101,
                B01111000,
                B00111110,
                B00101010,
                B01111110,
                B01110110,
                B01101110,
                B01011011,
                B00000000
            }; //alle segmentaufstellungen für buchstaben von a bis z (pgfedcba)
        bool
            number,
            high,
            low;
        unsigned int currentNumber;
        String text;

    public:

        SiebenSegment(bool ca, int* zifferPins, int* segmentPins);

        void
            run(void),
            init(void),
            drawNumber(int number, unsigned char decimal_point),
            drawNumber(int number),
            drawDecimal(unsigned char decimal_point),
            drawText(String text),
            setTextSpeed(int textSpeed);

};

#endif