#4-Fach Sieben Segment Anzeige für Arduino
Programm zur Ansteuerung einer 4-fach Sieben Segment Anzeige.
##Benutzung
Zuerst müssen die Dateien ``sieben_segment_treiber.h`` und ``sieben_segment_treiber.cpp`` ins Hauptverzeichnis des Arduino Sketches gelegt werden.

Die Datei ``sieben_segment_treiber.h`` muss mit ``#include "sieben_segment_treiber.h"`` eingebunden werden


Anschließend ein SiebenSegment Objekt Parametern erstellen:
```c++
SiebenSegment objekt(COMMON_CATHODE / COMMON_ANODE, ziffer_pins[4], segment_pins[8]);
```

In der ``void setup()`` Funktion muss das Objekt initialisiert werden.
```c++
objekt.init();
```

In der ``void loop()`` Funktion muss stets die Funktion ``objekt.run()`` ausgeführt werden.



Anschließend kann mit

* ``objekt.drawNumber(int Nummer);`` Eine Zahl "gemalt" werden
* ``objekt.drawDecimal(int Dezimalstelle);`` Der Dezimalpunkt gesetzt werden
* ``objekt.drawNumber(int Nummer, int Dezimalstelle);`` Eine Zahl "gemalt" und der Dezimalpunkt gesetzt werden
* ``objekt.drawText(String Text);`` Ein Text "gemalt" werden
* ``objekt.setTextSpeed(int Geschwindigkeit);`` Die Geschwindigkeit des Textes in ms geändert werden